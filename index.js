'use strict';

module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es6: true
    },
    parserOptions: {
        ecmaVersion: 2017,
        sourceType: 'module',
        ecmaFeatures: {
            experimentalObjectRestSpread: true,
        },
    },
    rules: {
        // The rules below are listed in the order they appear on the eslint
        // rules page. All rules are listed to make it easier to keep in sync
        // as new ESLint rules are added.
        // http://eslint.org/docs/rules/
        // - Rules in the `eslint:recommended` ruleset that aren't specifically
        //   mentioned by our styleguide are listed but commented out (so
        //   they don't override a base ruleset).
        // - Rules that are recommended but contradict our styleguide
        //   are explicitely set to the Google styleguide value.

        // Possible Errors
        // http://eslint.org/docs/rules/#possible-errors
        // ---------------------------------------------
        'no-await-in-loop': 'error',
        // 'no-cond-assign': 'error', // eslint:recommended
        'no-console': 'off',
        // eslint:recommended
        // 'no-constant-condition': 'error', // eslint:recommended
        // 'no-control-regex': 'error', // eslint:recommended
        // 'no-debugger': 'error', // eslint:recommended
        // 'no-dupe-args': 'error', // eslint:recommended
        // 'no-dupe-keys': 'error', // eslint:recommended
        // 'no-duplicate-case': 'error', // eslint:recommended
        // 'no-empty-character-class': 'error', // eslint:recommended
        // 'no-empty': 'error', // eslint:recommended
        // 'no-ex-assign': 'error', // eslint:recommended
        // 'no-extra-boolean-cast': 'error', // eslint:recommended
        // 'no-extra-parens': 'off',
        // 'no-extra-semi': 'error', // eslint:recommended
        // 'no-func-assign': 'error', // eslint:recommended
        // 'no-inner-declarations': 'error', // eslint:recommended
        // 'no-invalid-regexp': 'error', // eslint:recommended
        'no-irregular-whitespace': 'error',
        // eslint:recommended
        // 'no-obj-calls': 'error', // eslint:recommended
        // 'no-prototype-builtins': 'off',
        // 'no-regex-spaces': 'error', // eslint:recommended
        // 'no-sparse-arrays': 'error', // eslint:recommended
        'no-template-curly-in-string': 'error',
        'no-unexpected-multiline': 'error',
        // eslint:recommended
        // 'no-unreachable': 'error', // eslint:recommended
        // 'no-unsafe-finally': 'error', // eslint:recommended
        'no-unsafe-negation': 'error',
        // 'use-isnan': 'error' // eslint:recommended
        'valid-jsdoc': [
            'error',
            {
                'requireParamDescription': false,
                'requireReturnDescription': false,
                'requireReturn': false,
                'prefer': {
                    'returns': 'return'
                }
            }
        ],
        // 'valid-typeof': 'error' // eslint:recommended


        // Best Practices
        // http://eslint.org/docs/rules/#best-practices
        // --------------------------------------------

        // 'accessor-pairs': 'off',
        // 'array-callback-return': 'off',
        // 'block-scoped-var': 'off',
        // 'class-methods-use-this': 'off',
        // 'complexity': 'off',
        // 'consistent-return': 'off'
        'curly': 'error',
        // TODO(philipwalton): add an option to enforce braces with
        // the exception of simple, single-line if statements.
        'default-case': 'error',
        'dot-location': [
            'error',
            'property'
        ],
        'dot-notation': 'error',
        // 'eqeqeq': 'off',
        'guard-for-in': 'error',
        'no-alert': 'error',
        'no-caller': 'error',
        // 'no-case-declarations': 'error', // eslint:recommended
        // 'no-div-regex': 'off',
        // 'no-else-return': 'off',
        // 'no-empty-function': 'off',
        // 'no-empty-pattern': 'error', // eslint:recommended
        // 'no-eq-null': 'off',
        // 'no-eval': 'off',
        'no-extend-native': 'error',
        'no-extra-bind': 'error',
        // 'no-extra-label': 'off',
        // 'no-fallthrough': 'error', // eslint:recommended
        // 'no-floating-decimal': 'off',
        // 'no-global-assign': 'off',
        // 'no-implicit-coercion': 'off',
        // 'no-implicit-globals': 'off',
        // 'no-implied-eval': 'off',
        //  'no-invalid-this': 'off',
        // 'no-iterator': 'off',
        // 'no-labels': 'off',
        // 'no-lone-blocks': 'off',
        // 'no-loop-func': 'off',
        // 'no-magic-numbers': 'off',
        'no-multi-spaces': 'error',
        'no-multi-str': 'error',
        // 'no-new-func': 'off',
        'no-new-wrappers': 'error',
        // 'no-new': 'off',
        // 'no-octal-escape': 'off',
        // 'no-octal': 'error', // eslint:recommended
        // 'no-param-reassign': 'off',
        // 'no-proto': 'off',
        // 'no-redeclare': 'error', // eslint:recommended
        // 'no-return-assign': 'off',
        // 'no-script-url': 'off',
        // 'no-self-assign': 'error', // eslint:recommended
        // 'no-self-compare': 'off',
        // 'no-sequences': 'off',
        'no-throw-literal': 'error',
        // eslint:recommended
        // 'no-unmodified-loop-condition': 'off',
        // 'no-unused-expressions': 'off',
        // 'no-unused-labels': 'error', // eslint:recommended
        // 'no-useless-call': 'off',
        // 'no-useless-concat': 'off',
        // 'no-useless-escape': 'off',
        // 'no-void': 'off',
        // 'no-warning-comments': 'off',
        'no-with': 'error',
        // 'radix': 'off',
        // 'vars-on-top': 'off',
        // 'wrap-iife': 'off',
        // 'yoda': 'off',

        // Strict Mode
        // http://eslint.org/docs/rules/#strict-mode
        // -----------------------------------------
        // 'strict': 'off',

        // Variables
        // http://eslint.org/docs/rules/#variables
        // ---------------------------------------
        // 'init-declarations': 'off',
        // 'no-catch-shadow': 'off',
        // 'no-delete-var': 'error', // eslint:recommended
        // 'no-label-var': 'off',
        // 'no-restricted-globals': 'off',
        // 'no-shadow-restricted-names': 'off',
        // 'no-shadow': 'off',
        // 'no-undef-init': 'off',
        // 'no-undef': 'error', // eslint:recommended
        // 'no-undefined': 'off',
        'no-unused-vars': [
            'error',
            {
                'args': 'none'
            }
        ],
        // eslint:recommended
        // 'no-use-before-define': 'off',

        // Node.js and CommonJS
        // http://eslint.org/docs/rules/#nodejs-and-commonjs
        // -------------------------------------------------
        // 'callback-return': 'off',
        // 'global-require': 'off',
        // 'handle-callback-err': 'off',
        // 'no-mixed-requires': 'off',
        // 'no-new-require': 'off',
        // 'no-path-concat': 'off',
        // 'no-process-env': 'off',
        // 'no-process-exit': 'off',
        // 'no-restricted-modules': 'off',
        // 'no-restricted-properties': 'off',
        // 'no-sync': 'off',

        // Stylistic Issues
        // http://eslint.org/docs/rules/#stylistic-issues
        // ----------------------------------------------
        'array-bracket-spacing': [
            'error',
            'never'
        ],
        'block-spacing': [
            'error',
            'always'
        ],
        'brace-style': 'error',
        'camelcase': 'off',
        'comma-dangle': 'off',
        'comma-spacing': 'error',
        'comma-style': 'error',
        'computed-property-spacing': 'error',
        // 'consistent-this': 'off',
        'eol-last': 'error',
        'func-call-spacing': 'error',
        // 'func-name-matching': 'off',
        // 'func-names': 'off',
        // 'func-style': 'off',
        // 'id-blacklist': 'off',
        // 'id-length': 'off',
        // 'id-match': 'off',
        'indent': [
            'error',
            4,
            {
                'VariableDeclarator': {
                    'var': 1,
                    'let': 1
                }
            }
        ],
        // 'jsx-quotes': 'off',
        //        'key-spacing': [ TODO: Make this work
        //            'error',
        //            {
        //                'align': {
        //                    'beforeColon': true,
        //                    'afterColon': true,
        //                    'on': 'colon'
        //                }
        //            }
        //        ],
        'keyword-spacing': 'off',
        // 'line-comment-position': 'off',
        'linebreak-style': 'error',
        // 'lines-around-comment': 'off',
        // 'lines-around-directive': 'off',
        // 'max-depth': 'off',
        'max-len': [
            'error',
            {
                'code': 150,
                'comments': 150,
                'ignoreUrls': true
            }
        ],
        // 'max-lines': 'off',
        // 'max-nested-callbacks': 'off',
        // 'max-params': 'off',
        // 'max-statements-per-line': 'off',
        // 'max-statements': 'off',
        // 'multiline-ternary': 'off',
        'new-cap': 'error',
        // 'new-parens': 'off',
        // 'newline-after-var': 'off',
        // 'newline-before-return': 'off',
        // 'newline-per-chained-call': 'off',
        'no-array-constructor': 'error',
        // 'no-bitwise': 'off',
        // 'no-continue': 'off',
        // 'no-inline-comments': 'off',
        // 'no-lonely-if': 'off',
        // 'no-mixed-operators': 'off',
        'no-mixed-spaces-and-tabs': 'error',
        // eslint:recommended
        'no-multiple-empty-lines': [
            'error',
            {
                'max': 3
            }
        ],
        // 'no-negated-condition': 'off',
        // 'no-nested-ternary': 'off',
        'no-new-object': 'error',
        // 'no-plusplus': 'off',
        // 'no-restricted-syntax': 'off',
        // 'no-tabs': 'off',
        // 'no-ternary': 'off',
        'no-trailing-spaces': [
            'error',
            {
                'skipBlankLines': true
            }
        ],
        // 'no-underscore-dangle': 'off',
        // 'no-unneeded-ternary': 'off',
        // 'no-whitespace-before-property': 'off',
        // 'object-curly-newline': 'off',
        'object-curly-spacing': 'always',
        // 'object-property-newline': 'off',
        // 'one-var-declaration-per-line': 'off',
        'one-var': [
            'error',
            {
                'var': 'never',
                'let': 'never',
                'const': 'never'
            }
        ],
        // 'operator-assignment': 'off',
        // 'operator-linebreak': 'off',
        'padded-blocks': [
            'error',
            'never'
        ],
        'quote-props': [
            'error',
            'consistent'
        ],
        'quotes': [
            'error',
            'single',
            {
                'allowTemplateLiterals': true
            }
        ],
        'require-jsdoc': [
            'error',
            {
                'require': {
                    'FunctionDeclaration': true,
                    'MethodDefinition': true,
                    'ClassDeclaration': true
                }
            }
        ],
        'semi-spacing': 'error',
        'semi': 'error',
        // 'sort-keys': 'off',
        // 'sort-vars': 'off',
        'space-before-blocks': 'error',
        'space-before-function-paren': [
            'error',
            'never'
        ],
        // 'space-in-parens': 'off',
        // 'space-infix-ops': 'off',
        // 'space-unary-ops': 'off',
        'spaced-comment': [
            'error',
            'always'
        ],
        // 'unicode-bom': 'off',
        // 'wrap-regex': 'off',

        // ECMAScript 6
        // http://eslint.org/docs/rules/#ecmascript-6
        // ------------------------------------------
        // 'arrow-body-style': 'off',
        // 'arrow-parens': [
        //     'error',
        //     'always'
        // ],
        // 'arrow-spacing': 'off',
        'constructor-super': 'error',
        // eslint:recommended
        'generator-star-spacing': [
            'error',
            'after'
        ],
        // 'no-class-assign': 'off',
        // 'no-confusing-arrow': 'off',
        // 'no-const-assign': 'off', // eslint:recommended
        // 'no-dupe-class-members': 'off', // eslint:recommended
        // 'no-duplicate-imports': 'off',
        'no-new-symbol': 'error',
        // eslint:recommended
        // 'no-restricted-imports': 'off',
        'no-this-before-super': 'error',
        // eslint:recommended
        // 'no-useless-computed-key': 'off',
        // 'no-useless-constructor': 'off',
        // 'no-useless-rename': 'off',
        'no-var': 'error',
        // 'object-shorthand': 'off',
        // 'prefer-arrow-callback': 'off',
        // 'prefer-const': 'off',
        // 'prefer-numeric-literals': 'off',
        // 'prefer-reflect': 'off',
        'prefer-rest-params': 'error',
        'prefer-spread': 'error',
        // 'prefer-template': 'off',
        // 'require-yield': 'error', // eslint:recommended
        'rest-spread-spacing': 'error',
        // 'sort-imports': 'off',
        // 'symbol-description': 'off',
        // 'template-curly-spacing': 'off',
        'yield-star-spacing': [
            'error',
            'after'
        ]
    },
};
